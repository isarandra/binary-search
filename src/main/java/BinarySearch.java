import java.util.Scanner;

public class BinarySearch {
    static int dummy, count = 0;
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args){
        int[] list = {2,4,10,40,3};
        sort(list);
        printList(list);
        binarySearch(list);
    }

    private static void printList(int[] list) {
        System.out.print("[");
        for (int i=0;i<list.length;i++){
            if (i==list.length-1){
                System.out.print(list[i]);
            }
            else{
                System.out.print(list[i] + ", ");
            }
        }
        System.out.print("]");
    }

    private static void sort(int[] list) {
        if (count == list.length){
            return;
        }
        for(int i=1;i<list.length;i++){
            if(list[i-1]>list[i]){
                dummy=list[i-1];
                list[i-1]=list[i];
                list[i]=dummy;
            }
        }
        count++;
        sort(list);
    }

    private static void binarySearch(int[] list) {
        System.out.print("\nMasukkan angka yang ingin dicari indeksnya: ");
        int targetNumber = scanner.nextInt();
        int leftMostIndex = 0;
        int rightMostIndex = list.length - 1;
        int resultIndex = 0;
        while(leftMostIndex<=rightMostIndex){
            int middleIndex = (leftMostIndex+rightMostIndex)/2;
            if(targetNumber<=list[middleIndex]){
                rightMostIndex=middleIndex-1;
                resultIndex = middleIndex;
            }
            else{
                leftMostIndex=middleIndex+1;
            }
        }
        if(targetNumber==list[resultIndex]){
            System.out.println(targetNumber+" ada di indeks: "+resultIndex);
        }
        else{
            System.out.println(targetNumber+" tidak ada di dalam list");
        }
    }
}
